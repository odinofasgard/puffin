//
//  ContentView.swift
//  Puffin
//
//  Created by Mattias Burstrom on 4/17/23.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        VStack {
            Image("puffin")
                .imageScale(.large)
                .foregroundColor(.accentColor)
            Text("I'm PUFFIN!")
                .font(.system(size: 50))
                .fontWeight(.bold)
                .foregroundColor(.orange)
        }
        .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
