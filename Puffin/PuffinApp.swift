//
//  PuffinApp.swift
//  Puffin
//
//  Created by Mattias Burstrom on 4/17/23.
//

import SwiftUI

@main
struct PuffinApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
